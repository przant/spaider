package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/apem/spaider/arrgs"
	"gitlab.com/apem/spaider/gloop"
	"gitlab.com/apem/spaider/kroller"
	"gitlab.com/apem/spaider/vook"
)

func main() {
	var links []url.URL

	start := time.Now()
	seeds := kroller.ParseSeeds()
	for _, seed := range seeds {
		lks, err := kroller.Krol(seed)
		if err != nil {
			continue
		}
		links = append(links, lks...)
	}
	if len(links) == 0 {
		log.Fatalf("Error urls iniciales no contienen enlaces.\n")
	}
	go func(lks []url.URL) {
		gloop.Pendings <- lks
	}(links)

	for {
		select {
		case urls := <-gloop.Pendings:
			for _, u := range urls {
				if !kroller.InDomain(u) {
					continue
				}

				uclean := kroller.CleanURL(u)
				uhost := uclean.Hostname()
				urlstr := uclean.String()

				switch {
				case uclean.Scheme == "http":
					urlstr = strings.TrimPrefix(urlstr, "http://"+uhost)
				case uclean.Scheme == "https":
					urlstr = strings.TrimPrefix(urlstr, "https://"+uhost)
				}

				if _, hv := kroller.Cache[uhost]; !hv {
					hostpath := arrgs.GetBakPath() + uhost
					err := os.MkdirAll(hostpath, os.ModeDir|os.ModePerm)
					if err != nil {
						if vook.ErrsFile {
							fmt.Fprintf(vook.ErrsLog, "Error al crear dir %q: %s\n", hostpath, err)
						} else {
							fmt.Fprintf(os.Stderr, "Error al crear dir %q: %s\n", hostpath, err)
						}
					}
					kroller.Cache[uhost] = make(map[string]bool)
				}

				if _, lv := kroller.Cache[uhost][urlstr]; !lv {
					kroller.Links++
					kroller.Cache[uhost][urlstr] = true
					go func(link url.URL) { gloop.Worklist <- link }(uclean)
				}
			}
		case <-gloop.Quite:
			vook.LinksLog.Close()
			vook.ErrsLog.Close()
			dls := countDL()
			fmt.Println()
			fmt.Fprintf(os.Stdout, "Inicio : %s\n", start.Format(time.Stamp))
			fmt.Fprintf(os.Stdout, "Termino: %s\n\n",
				time.Now().Add(-arrgs.GetDelay()).Format(time.Stamp))
			fmt.Fprintf(os.Stdout, "Total host encontrados %d\n", len(kroller.Cache))
			fmt.Fprintf(os.Stdout, "Total links encontrados %d\n", kroller.Links)
			fmt.Fprintf(os.Stdout, "Total links descargados %d\n\n", dls)
			fmt.Fprintf(os.Stdout, "Porcentaje de descargas %.2f%%\n\n",
				100.00*float64(dls)/float64(kroller.Links))
			fmt.Fprintf(os.Stdout, "Tiempo total %s\n",
				time.Since(start.Add(arrgs.GetDelay())).Truncate(time.Millisecond))
			return
		}
	}
}

func countDL() (dls int64) {
	bakdir := arrgs.GetBakPath()
	list, err := ioutil.ReadDir(bakdir)
	if err != nil {
		dls = 0
		fmt.Fprintf(os.Stderr, "Error al abrir directorio %q: %s\n", bakdir, err)
		return dls
	}
	for _, elt := range list {
		if dir := elt.IsDir(); dir {
			host, err := ioutil.ReadDir(arrgs.GetBakPath() + elt.Name())
			if err != nil {
				continue
			}
			dls += int64(len(host))
		}
	}
	return dls
}
