package gloop

import (
	"fmt"
	"net/url"
	"os"
	"sync"
	"time"

	"gitlab.com/apem/spaider/vook"

	"gitlab.com/apem/spaider/arrgs"

	"gitlab.com/apem/spaider/kroller"
)

var (
	// Pendings canal de comunicación que transporta las urls encontradas y que
	// estan pedientes de ser verificadas y procesadas.
	Pendings chan []url.URL

	// Quite canal de comunicación que sirve para el envío de la señal de
	// terminación del programa.
	Quite chan bool

	// Worklist canal de comunicación que transporta las urls que no han sido
	// visitadas aún, para realizar parseo y extraccion de links.
	Worklist chan url.URL
)

func init() {
	Pendings = make(chan []url.URL)
	Quite = make(chan bool)
	Worklist = make(chan url.URL)
	go Sentinel()
	go WorkerPool(Pendings, Worklist)
}

// Sentinel . . . . .
func Sentinel() {
	mu := new(sync.Mutex)

	loops := 1
	limit := arrgs.GetChecks()

	mu.Lock()
	oldCount := kroller.Links
	mu.Unlock()
	newCount := int64(0)

	interval := arrgs.GetDelay() / time.Duration(arrgs.GetChecks())

	for {
		time.Sleep(interval)
		mu.Lock()
		newCount = kroller.Links
		mu.Unlock()
		switch {
		case oldCount == newCount && loops < limit:
			loops++
		case oldCount == newCount && loops == limit:
			Quite <- true
		default:
			oldCount, newCount, loops = newCount, 0, 1
		}
	}
}

// WorkerPool . . . . .
func WorkerPool(pends chan<- []url.URL, workls <-chan url.URL) {
	workers := arrgs.GetNumTask()
	for worker := 1; worker <= workers; worker++ {
		go Worker(pends, workls)
	}
}

// Worker . . . . .
func Worker(pendsCh chan<- []url.URL, worklsCh <-chan url.URL) {
	for u := range worklsCh {
		links, err := kroller.Krol(u)
		if err != nil {
			if vook.ErrsFile {
				fmt.Fprintf(vook.ErrsLog, "Error %s\n", err)
				continue
			} else {
				fmt.Fprintf(os.Stdout, "Error %s\n", err)
				continue
			}
		}

		nbytes, err := kroller.Fetch(u)
		if err != nil {
			if vook.ErrsFile {
				fmt.Fprintf(vook.ErrsLog, "Error %s\n", err)
				continue
			} else {
				fmt.Fprintf(os.Stderr, "Error %s\n", err)
				continue
			}
		}
		if vook.IndxFile {
			fmt.Fprintf(vook.LinksLog, "%q (%d)\n", u.String(), nbytes)
		} else {
			fmt.Fprintf(os.Stdout, "%q (%d)\n", u.String(), nbytes)
		}
		pendsCh <- links
	}
}
