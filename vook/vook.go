package vook

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/apem/spaider/arrgs"
)

const (
	urlsfile = "urls.log"

	errsfile = "errors.log"
)

var (
	// LinksLog referencia al archivo, en el cual se registrarán las urls
	// visitadas durante el proceso de web crawling
	LinksLog *os.File

	// ErrsLog refrencia al archivo, en el cual se registrarán los errores
	// se generen durante el proceso de web crawling.
	ErrsLog *os.File
)

var (
	// IndxFile indica si el archivo bitacora de urls encontradas y descargadas
	// fue creado con éxito.
	IndxFile bool
	// ErrsFile indica si el archivo bitacora de errores, que aparecieron en el
	// proceso de crawling, fue creado con éxito.
	ErrsFile bool
)

func init() {
	if idxlog, err := createLinkLogger(); err != nil {
		log.Printf("Error %s\n", err)
	} else {
		IndxFile = true
		LinksLog = idxlog
	}

	if errlog, err := createErrorLogger(); err != nil {
		log.Printf("Error %s\n", err)
	} else {
		ErrsFile = true
		ErrsLog = errlog
	}
}

func createLinkLogger() (index *os.File, err error) {
	bakPath := arrgs.GetBakPath() + urlsfile
	flags := os.O_CREATE | os.O_WRONLY | os.O_APPEND
	perms := os.ModeAppend | os.ModePerm

	file, err := os.Create(bakPath)
	if err != nil {
		index = nil
		err = fmt.Errorf("al crear bitacora de descargas: %s", err)
		return
	}
	file.Close()
	index, err = os.OpenFile(bakPath, flags, perms)
	if err != nil {
		index = nil
		err = fmt.Errorf("al abrir bitacora de descargas: %s", err)
		return
	}

	return
}

func createErrorLogger() (errFile *os.File, err error) {
	bakPath := arrgs.GetBakPath() + errsfile
	flags := os.O_CREATE | os.O_WRONLY | os.O_APPEND
	perms := os.ModeAppend | os.ModePerm

	file, err := os.Create(bakPath)
	if err != nil {
		errFile = nil
		err = fmt.Errorf("al crear bitacora de errores: %s", err)
		return
	}
	file.Close()
	errFile, err = os.OpenFile(bakPath, flags, perms)
	if err != nil {
		errFile = nil
		err = fmt.Errorf("al abrir bitacora de errores: %s", err)
		return
	}

	return
}
