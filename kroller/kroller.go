package kroller

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"

	"gitlab.com/apem/spaider/arrgs"

	"golang.org/x/net/html"
)

const (
	// anchor es el valor a comparar con el campo Data de un Token html
	anchor = "a"
	// homePath es la ruta absoluta a la página inicial de un sitio
	homePath = "/"
	// hrefAttr atributo que debe de tener un Token html en su campo Attr
	hrefAttr = "href"
)

const (
	cleaner = "https?://[[:alnum:]\\.:/_-]*(\\?([[:alnum:]=\\./_-]+)(&[[:alnum:]=\\._-]+)?)?"
)

var (
	// Links . . . . .
	Links int64
)

var (
	// Cache . . . . .
	Cache map[string]map[string]bool
)

var (
	urlCleaner *regexp.Regexp
)

func init() {
	Links = 0
	Cache = make(map[string]map[string]bool)
	urlCleaner = regexp.MustCompile(cleaner)

}

// isAnchorTag verifica si el token html t tiene atributo href, y en caso
// de que lo tenga devuelve el valor asociado al atributo y true. En caso
// contrario devuelve cadena vacia y false.
func isAnchorTag(t html.Token) (isAnchor bool) {
	isAnchor = t.Data == anchor
	return
}

// CleanURL válida la url, de ser necesario la trunca a una cadena válida
// para ser utilizadad en la búsqueda y descarga de sitios.
func CleanURL(u url.URL) (uclean url.URL) {
	urltrunc := urlCleaner.FindString(u.String())
	cleanURL, err := url.Parse(urltrunc)
	if err != nil {
		if u.Path == "" {
			u.Path = homePath
		}
		u.RawPath = ""
		u.Fragment = ""
		return u
	}
	if cleanURL.Path == "" {
		cleanURL.Path = homePath
	}
	cleanURL.RawPath = ""
	cleanURL.Fragment = ""
	return *cleanURL
}

// InDomain verifica si la url pertenece al dominio deseado, descartando
// aquellas urls que a pesar de pertenecer al dominio, son parte de la
// lista de dominios no deseados. Si la url es válida se devuelve true,
// caso contrario se devuelve false.
func InDomain(u url.URL) (inDom bool) {
	dom := arrgs.GetDomain()
	if isEmpty := arrgs.BlackListsEmpty(); isEmpty {
		// Verifica pertenencia de dominio por hostname
		inDom = strings.Contains(u.Hostname(), dom)
	} else {
		// Verifica pertenencia de dominio por hostname
		if strings.Contains(u.Hostname(), dom) {
			// Valida que el hostname no sea parte de dominios inválidos
			for _, badDom := range arrgs.GetBlackls() {
				if strings.Contains(u.Hostname(), badDom) {
					inDom = false
					return
				}
			}
			inDom = true
		}
	}
	return
}

// ConnecTo estabelece la conexión con la url u y devuelve la respuesta y
// un error nulo. Si se genera error al establecer conexión con el sitio o
// el sitio responde con un código diferente de 200, se devuelve una nulo
// y el error que surgió.
func ConnecTo(u url.URL) (r *http.Response, err error) {
	r, err = http.Get(u.String())
	// Si surgio error al establecer conexión
	if err != nil {
		r = nil
		return r, err
	}
	// Si el sitio responde con código diferente de 200
	if r.StatusCode != http.StatusOK {
		err = fmt.Errorf("sitio %q responde: %s", u.String(), r.Status)
		r.Body.Close()
		r = nil
		return r, err
	}
	err = nil
	return r, err
}

// GetAttr obtiene el valor del atributo atr si existe en el token t,
// si el atributo atr no existe en el token GetAttr devuelve cadena vacia
// y false, caso contrario devuelve el valor del atributo y true.
func GetAttr(atr string, t html.Token) (val string, exist bool) {
	for _, attr := range t.Attr {
		if attr.Key == atr {
			exist = true
			val = attr.Val
		}
	}
	return
}

// GetLink devuelve una url procesada a partir de la url base asociada a
// la respuesta http y error nulo. Si existe algún error en el parseo,
// GetLink devuelve url nula y el error generado.
func GetLink(href string, r *http.Response) (u *url.URL, err error) {
	u, err = r.Request.URL.Parse(href)
	return
}

// ParseSeeds . . . . .
func ParseSeeds() (urls []url.URL) {
	// Obtiene la lista de urls iniciales
	seeds := arrgs.GetSeedList()

	// Parse de cadena a estructura url.URL para cada url inicial
	for _, seed := range seeds {
		seedURL, _ := url.Parse(seed)
		urls = append(urls, *seedURL)
	}
	return
}

// ParseBody realiza el procesado del contenido del sitio al que apunta la
// respuesta http 'r', devuelve una lista de todos los enlaces que se
// encuentran en el contenido del sitio, en forma de estructura url.URL.
func ParseBody(r *http.Response) (links []url.URL) {
	// Crea tokenizador a partir del cuerpo asociado a la respuesta
	tzr := html.NewTokenizer(r.Body)

	for {
		// Obtiene el tipo del token sig.
		tt := tzr.Next()

		switch {
		case tt == html.StartTagToken: // Token de apertura
			var href string
			tkn := tzr.Token()
			// Se verifica si el token esta asociado a una etiqueta ancla
			if ok := isAnchorTag(tkn); !ok {
				continue
			}
			// Se obtiene el valor del atributo href de la etiqueta ancla
			if attrVal, exist := GetAttr(hrefAttr, tkn); !exist {
				continue
			} else {
				href = attrVal
			}
			// Se realiza parseo, tomando como base la url actual
			if link, err := GetLink(href, r); err != nil {
				continue
			} else {
				links = append(links, *link)
			}
		case tt == html.ErrorToken: // Final del documento
			return links
		default:
			continue
		}
	}
}

// Fetch . . . . .
func Fetch(u url.URL) (bytes int64, err error) {
	var filename string
	resp, err := ConnecTo(u)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if arrgs.GetOnlyHTML() {
		content := resp.Header.Get("content-type")
		if !strings.Contains(content, "text/html") {
			err = fmt.Errorf("%q solo se desea contenido text/html: %s",
				u.String(), content)
			return 0, err
		}
	}

	switch {
	case u.Path == homePath || u.Path == "":
		filename = "index_home.html"
	case u.RawQuery != "":
		urlpath := strings.Replace(u.Path, "/", ".", -1)
		filename = strings.TrimPrefix(strings.TrimSuffix(urlpath, "."), ".") + "_" + url.QueryEscape(u.RawQuery)
	default:
		urlpath := strings.Replace(u.Path, "/", ".", -1)
		filename = strings.TrimPrefix(strings.TrimSuffix(urlpath, "."), ".")
	}
	filepath := arrgs.GetBakPath() + u.Hostname() + homePath
	// Valida si la ruta existe, sino la configura al valor de la ruta
	// de línea de comandos.
	if _, err := os.Open(filepath); err != nil {
		if os.IsNotExist(err) {
			filepath = arrgs.GetBakPath()
		}
	}
	file, err := os.Create(filepath + filename)
	if err != nil {
		err = fmt.Errorf("al crear archivo %q: %s", filename, err)
		return 0, err
	}
	defer file.Close()
	nbytes, err := io.Copy(file, resp.Body)
	if err != nil {
		err = fmt.Errorf("durante la copia de %q: %s", u.String(), err)
		return 0, err
	}
	return nbytes, nil
}

// Krol devuelve una lista de urls, las cuales ya se procesaron con base
// en la url u, y error nulo. Si surgio error en la conexión con u o en la
// respuesta del sitio al que apunta u, se regresa lista vacía y el error.
func Krol(u url.URL) (urls []url.URL, err error) {
	// Se intenta extablecer conexión con u
	resp, err := ConnecTo(u)
	// Si surgio error
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	// Obtencion de los links contenidos en la página
	urls = ParseBody(resp)
	return urls, nil
}
